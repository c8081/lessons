'''
An array A consisting of N integers is given. A triplet (P, Q, R) is triangular if 0 ≤ P < Q < R < N and:

A[P] + A[Q] > A[R],
A[Q] + A[R] > A[P],
A[R] + A[P] > A[Q].
For example, consider array A such that:

  A[0] = 10    A[1] = 2    A[2] = 5
  A[3] = 1     A[4] = 8    A[5] = 20
Triplet (0, 2, 4) is triangular.

Write a function:

def solution(A)

that, given an array A consisting of N integers, returns 1 if there exists a triangular triplet for this array and returns 0 otherwise.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [0..100,000];
each element of array A is an integer within the range [−2,147,483,648..2,147,483,647].
'''

# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def checkTriangular(a,b,c):
    ab = a+b > c
    ac = a+c > b
    bc = b+c > a
    if (ab and ac and bc):
        return 1
    else:
        return 0

def solution(A):
    # write your code in Python 3.6
    n = len(A)
    A.sort()

    for i in range(n-2):
        if (checkTriangular(A[i],A[i+1],A[i+2]) == 1):
            return 1
    return 0

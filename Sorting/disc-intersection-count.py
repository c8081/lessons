'''
We draw N discs on a plane. The discs are numbered from 0 to N − 1. An array A of N non-negative integers, specifying the radiuses of the discs, 
is given. The J-th disc is drawn with its center at (J, 0) and radius A[J].

We say that the J-th disc and K-th disc intersect if J ≠ K and the J-th and K-th discs have at least one common point 
(assuming that the discs contain their borders).

Write a function:

def solution(A)

that, given an array A describing N discs as explained above, returns the number of (unordered) pairs of intersecting discs. 
The function should return −1 if the number of intersecting pairs exceeds 10,000,000.

Write an efficient algorithm for the following assumptions:
	N is an integer within the range [0..100,000];
	Each element of array A is an integer within the range [0..2,147,483,647].
'''

# We only need to know how many discs are open when a disc closes.
# Thus we can step through all the closing points and simply count the number of 
# discs that have opened since the last close.

# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(A):
    # write your code in Python 3.6
    n = len(A)
    intersections = 0
    upper = [0]*n
    lower = [0]*n

    for i in range(n):
        upper[i] = i + A[i]
        lower[i] = i - A[i]
    
    upper.sort()
    lower.sort()

    low = 0
    for up in range(n):
        while(low < n and lower[low] <= upper[up]):
            low += 1
        intersections += (low - 1 ) - up

        if (intersections > 10000000):
            return -1

    return intersections

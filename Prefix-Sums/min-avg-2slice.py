'''
A non-empty array A consisting of N integers is given. A pair of integers (P, Q), such that 0 ≤ P < Q < N, is called a slice of array A 
(notice that the slice contains at least two elements). The average of a slice (P, Q) is the sum of A[P] + A[P + 1] + ... + A[Q] divided by 
the length of the slice. To be precise, the average equals (A[P] + A[P + 1] + ... + A[Q]) / (Q − P + 1).

For example, array A such that:

    A[0] = 4
    A[1] = 2
    A[2] = 2
    A[3] = 5
    A[4] = 1
    A[5] = 5
    A[6] = 8
contains the following example slices:

slice (1, 2), whose average is (2 + 2) / 2 = 2;
slice (3, 4), whose average is (5 + 1) / 2 = 3;
slice (1, 4), whose average is (2 + 2 + 5 + 1) / 4 = 2.5.
The goal is to find the starting position of a slice whose average is minimal.

Write a function:

def solution(A)

that, given a non-empty array A consisting of N integers, returns the starting position of the slice with the minimal average. 
If there is more than one slice with a minimal average, you should return the smallest starting position of such a slice.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [2..100,000];
each element of array A is an integer within the range [−10,000..10,000].
'''

# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(A):
    # write your code in Python 3.6


    # NOTE: You do not need to look at ALL subarrays - just sub-arrays of length 2 or 3
    
    n = len(A)
    index = 0
    min_avg = 10000

    for i in range(n-2):
        avg2 = (A[i]+A[i+1])/2.0
        avg3 =(A[i]+A[i+1] + A[i+2])/3.0

        if (avg2 < min_avg):
            min_avg = avg2
            index = i

        if (avg3 < min_avg):
            min_avg = avg3
            index = i

    # look at average of last two elements - i.e. index = n-2 , n-1
    last = n-2
    if ((A[last]+A[last+1])/2.0 < min_avg):
        index = last
    
    return index

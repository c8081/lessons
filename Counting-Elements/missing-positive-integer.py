'''
Write a function:

def solution(A)

that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.

For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

Given A = [1, 2, 3], the function should return 4.

Given A = [−1, −3], the function should return 1.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [1..100,000];
each element of array A is an integer within the range [−1,000,000..1,000,000].
'''

# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(A):
    # write your code in Python 3.6
    smallest = 1
    distinct_set = set(A)
    range_set = set(range(1,len(distinct_set)+1))
    
    difference_set = range_set.difference(distinct_set)
    diff = list(difference_set)

    if (len(diff) > 0):
        smallest = min(diff)
    else:
        smallest = len(range_set)+1

    return smallest